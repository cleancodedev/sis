﻿using System;
using System.IO;
using contestrunner.contract.host;
using contest.submission.contract;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Globalization;
using System.Diagnostics;

namespace contest.host
{

  public class Dnp1510Host : IHost
  {
    string encryptedtext = "";
    string key = "";
    Stopwatch stopwatch = new Stopwatch();

    
    public void Prüfen(object beitrag, string wettbewerbspfad, string beitragsverzeichnis)
    {
      var sut = (IDnp1510Solution)beitrag;
      var steps = new List<Point>();

      
      stopwatch.Start();

      sut.Result += (x) => Console.WriteLine("Entschlüsselter Text: {0}   Schlüssel: {1}", x.Text, x.Key);
      
      var anfang = new Prüfungsanfang { Wettbewerb = Path.GetFileName(wettbewerbspfad), Beitrag = Path.GetFileName(beitragsverzeichnis) };
      Anfang(anfang);
      sut.Process(encryptedtext, key.Length);

      stopwatch.Stop();

      Status(new Prüfungsstatus() { Statusmeldung = "" });
      Ende(new Prüfungsende(){ Dauer = stopwatch.Elapsed });

    }

    public event Action<Prüfungsanfang> Anfang;
    public event Action<Prüfungsstatus> Status;
    public event Action<Prüfungsende> Ende;
    public event Action<Prüfungsfehler> Fehler;
  }

 
}
