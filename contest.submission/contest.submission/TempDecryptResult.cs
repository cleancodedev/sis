﻿using contest.submission.contract;

namespace contest.submission
{
    public class TempDecryptResult
    {
        public TempDecryptResult(DecryptResult decryptResult, double plausability)
        {
            DecryptResult = decryptResult;
            Rate = plausability;
        }

        public double Rate { get; set; }
        public DecryptResult DecryptResult { get; set; }
    }
}