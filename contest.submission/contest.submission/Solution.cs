﻿using System;
using contest.submission.contract;
using contest.submission.Splitting;

namespace contest.submission
{

    [Serializable]
    public class Solution : IDnp1510Solution
    {
        public void Process(string cipherText, int keylength)
        {
            var decryptor = new Decryptor();
            var result = decryptor.Decipher(cipherText, keylength);

            Result(result);
        }

        public event Action<DecryptResult> Result;
    }
}
