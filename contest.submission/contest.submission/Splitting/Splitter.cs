﻿namespace contest.submission.Splitting
{
    public class Splitter : ISplit
    {
        public string[] Split(string text, int length)
        {
            if(string.IsNullOrEmpty(text))
            {
                return new string[0];
            }

            var result = new string[length];
            for (int i = 0; i < text.Length; i++)
            {
                int mod = i % length;
                result[mod] += text[i];
            }

            return result;
        }
    }
}
