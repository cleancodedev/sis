﻿namespace contest.submission.Splitting
{
    public interface ISplit
    {
        string[] Split(string text, int length);
    }
}