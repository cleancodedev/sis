﻿using System.Collections.Generic;

namespace contest.submission.Analyzers
{
    public class BigramAnalyzer : ITextAnalyzer
    {
        public double Analyze(string text)
        {
            return Analyze(text, 0.15);
        }

        public double Analyze(string text, double thresholdFactor)
        {
            var results = new Dictionary<string, double>();
            double plausibility = 0;
            foreach (var frequency in StatisticNumbers.BigramOccurencyList)
            {
                results.Add(frequency.Key, CharUtils.CountString(text, frequency.Key) / (text.Length * 1.00));
            }

            foreach (var result in results)
            {
                var upperLimit = StatisticNumbers.BigramOccurencyList[result.Key] * (1 + thresholdFactor);
                var lowerLimit = StatisticNumbers.BigramOccurencyList[result.Key] * (1 - thresholdFactor);

                if (result.Value <= upperLimit && result.Value >= lowerLimit)
                {
                    plausibility += StatisticNumbers.BigramOccurencyList[result.Key];
                }
            }

            return plausibility;
        }
    }
}
