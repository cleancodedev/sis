﻿using System;
using System.Collections.Generic;
using contest.submission.Properties;

namespace contest.submission.Analyzers
{
    public class DictAnalyzer : ITextAnalyzer
    {
        public double Analyze(string text)
        {
            return Analyze(text, 0.15);
        }

        public double Analyze(string text, double thresholdFactor)
        {
            double plausibility = 0;

            var matched = 0;
            var matchedWords = new List<string>();

            foreach (var currentWord in StatisticNumbers.GermanDictionary)
            {
                if (text.IndexOf(currentWord, StringComparison.CurrentCultureIgnoreCase) != -1)
                {
                    matched++;
                    matchedWords.Add(currentWord);
                    if (matched > 2)
                    {
                        break;
                    }
                }
            }

            if (matched > 3)
            {
                printOut(matchedWords);
                plausibility = 1;
            }
            else if (matched > 2)
            {
                printOut(matchedWords);
                plausibility = 0.8;
            }
            else if (matched > 0)
            {
                plausibility = 0.4;
            }

            return plausibility;
        }

        public void printOut(List<string> input)
        {
            input.ForEach(s => Console.WriteLine(s));
        }
    }
}
