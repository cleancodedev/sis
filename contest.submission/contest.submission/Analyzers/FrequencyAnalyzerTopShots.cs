﻿namespace contest.submission.Analyzers
{
    public class FrequencyAnalyzerTopShots : ITextAnalyzer
    {
        public double Analyze(string text)
        {
            return Analyze(text, 0.15);
        }

        public double Analyze(string text, double thresholdFactor)
        {
            double plausibility = 0;
            foreach (var frequency in StatisticNumbers.CharOccurencyList)
            {
                if (text.Contains(frequency.Key.ToString()))
                {
                    plausibility += frequency.Value;
                }
            }

            return plausibility;
        }
    }
}