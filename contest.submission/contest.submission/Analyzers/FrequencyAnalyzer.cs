﻿using System.Collections.Generic;

namespace contest.submission.Analyzers
{
    public class FrequencyAnalyzer : ITextAnalyzer
    {
        public double Analyze(string text)
        {
            return Analyze(text, 0.15);
        }

        public double Analyze(string text, double thresholdFactor)
        {
            var results = new Dictionary<char, double>();
            double plausibility = 0;
            foreach (var frequency in StatisticNumbers.CharOccurencyList)
            {
                results.Add(frequency.Key, CharUtils.CountChar(text, frequency.Key) / (text.Length * 1.00));
            }

            foreach (var result in results)
            {
                var upperLimit = StatisticNumbers.CharOccurencyList[result.Key] * (1 + thresholdFactor);
                var lowerLimit = StatisticNumbers.CharOccurencyList[result.Key] * (1 - thresholdFactor);

                if (result.Value <= upperLimit && result.Value >= lowerLimit)
                {
                    plausibility += StatisticNumbers.CharOccurencyList[result.Key];
                }
            }

            return plausibility;
        }
    }
}