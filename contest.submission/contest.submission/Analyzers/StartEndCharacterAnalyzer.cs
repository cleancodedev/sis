﻿using System.Collections.Generic;

namespace contest.submission.Analyzers
{
    public class StartEndCharacterAnalyzer : ITextAnalyzer
    {
        public double Analyze(string text)
        {
            return Analyze(text, 0.15);
        }

        public double Analyze(string text, double thresholdFactor)
        {
            var results = new List<double>();
            foreach (var frequency in StatisticNumbers.StartCharOccurencyList)
            {
                if (text.StartsWith(frequency.Key.ToString()))
                {
                    results.Add(frequency.Value);
                    break;
                }
            }

            foreach (var frequency in StatisticNumbers.EndCharOccurencyList)
            {
                if (text.EndsWith(frequency.Key.ToString()))
                {
                    results.Add(frequency.Value);
                    break;
                }
            }

            double plausibility = 0;

            foreach (var result in results)
            {
                plausibility += result;
            }
            return plausibility;
        }
    }
}