namespace contest.submission.Analyzers
{
    public interface ITextAnalyzer
    {
        double Analyze(string text);
        double Analyze(string text, double thresholdFactor);

    }
}