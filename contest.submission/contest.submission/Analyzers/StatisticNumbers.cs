using System;
using System.Collections.Generic;

namespace contest.submission.Analyzers
{
    public static class StatisticNumbers
    {
        public static Dictionary<char, double> CharOccurencyList = new Dictionary<char, double>();
        public static Dictionary<char, double> StartCharOccurencyList = new Dictionary<char, double>();
        public static Dictionary<char, double> EndCharOccurencyList = new Dictionary<char, double>();
        public static Dictionary<string, double> BigramOccurencyList = new Dictionary<string, double>();

        public static string[] GermanDictionary = Properties.Resources.german.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

        static StatisticNumbers()
        {
            //CharOccurency
            CharOccurencyList.Add('E', 0.174);
            CharOccurencyList.Add('N', 0.0978);
            CharOccurencyList.Add('I', 0.0755);
            CharOccurencyList.Add('S', 0.0727);
            CharOccurencyList.Add('R', 0.0700);
            CharOccurencyList.Add('A', 0.0651);
            CharOccurencyList.Add('T', 0.0615);
            CharOccurencyList.Add('D', 0.0508);
            CharOccurencyList.Add('H', 0.0476);
            CharOccurencyList.Add('U', 0.0435);
            CharOccurencyList.Add('L', 0.0344);
            CharOccurencyList.Add('C', 0.0306);
            CharOccurencyList.Add('G', 0.0301);
            CharOccurencyList.Add('M', 0.0253);
            CharOccurencyList.Add('O', 0.0251);

            //StartCharOccurency
            StartCharOccurencyList.Add('D', 0.142);
            StartCharOccurencyList.Add('S', 0.108);
            StartCharOccurencyList.Add('E', 0.078);
            StartCharOccurencyList.Add('I', 0.071);
            StartCharOccurencyList.Add('W', 0.068);

            //EndCharOccurency
            EndCharOccurencyList.Add('N', 0.21);
            EndCharOccurencyList.Add('E', 0.151);
            EndCharOccurencyList.Add('R', 0.13);
            EndCharOccurencyList.Add('T', 0.103);
            EndCharOccurencyList.Add('S', 0.096);

            //BigramOccurencyList
            BigramOccurencyList.Add("ER", 0.06555);
            BigramOccurencyList.Add("EN", 0.06045);
            BigramOccurencyList.Add("CH", 0.04693);
            BigramOccurencyList.Add("DE", 0.03763);
            BigramOccurencyList.Add("EI", 0.03204);
            BigramOccurencyList.Add("IN", 0.03170);
            BigramOccurencyList.Add("TE", 0.03066);
            BigramOccurencyList.Add("IE", 0.02668);
            BigramOccurencyList.Add("GE", 0.02324);
            BigramOccurencyList.Add("ND", 0.02211);
            BigramOccurencyList.Add("UN", 0.02121);
            BigramOccurencyList.Add("BE", 0.01983);
        }
    }
}