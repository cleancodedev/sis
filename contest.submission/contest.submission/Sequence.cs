﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using contest.submission.Strategies;

namespace contest.submission
{
    public class Sequence
    {
        private readonly int _keyLength;
        private readonly string _sequenceText;

        public Sequence(string sequenceText, int keyLength)
        {
            this._sequenceText = sequenceText;
            this._keyLength = keyLength;
        }

        public string SequenceText
        {
            get { return _sequenceText; }
        }

        public char Analyze(IStrategy strategy)
        {
            return strategy.Analyze(this._sequenceText, this._keyLength);
        }
    }
}
