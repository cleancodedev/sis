﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using contest.submission.Analyzers;
using contest.submission.contract;
using contest.submission.Splitting;
using contest.submission.Strategies;

namespace contest.submission
{
    public class Decryptor
    {
        public DecryptResult Decipher(string cipherText, int keyLength)
        {
             

            var tempDecryptResults = new List<TempDecryptResult>();

            var stopWatch = Stopwatch.StartNew();
            IStrategy currentStrategy = null;
            var splitter = new Splitter();
            var sequenceTexts = splitter.Split(cipherText, keyLength);

            List<Sequence> sequences = new List<Sequence>();
            for (int i = 0; i < sequenceTexts.Length; i++)
            {
                sequences.Add(new Sequence(sequenceTexts[i], keyLength));
            }

            //per 1 keyChar, 29 textChar needed
            //if the ratio is lower, the TopShot Analyze gets more weight..
            var topShotFactor = 0.2;
            var frequencyFactor = 0.2;
            var bigramFactor = 0.03;
            var startEndFactor = 0.03;

            if ((cipherText.Length / keyLength) < 29)
            {
                frequencyFactor = frequencyFactor*0.75;
            }
            else
            {
                topShotFactor = topShotFactor*0.75;
            }

            if (cipherText.Length > 500)
            {
                bigramFactor = 0.06;
            }

            var cipherDecrypted = false;
            var tryAllMostUsedChars = false;
            Dictionary<char, double>.Enumerator mostUsedCurrentChar = StatisticNumbers.CharOccurencyList.GetEnumerator();
            mostUsedCurrentChar.MoveNext();

            while (!cipherDecrypted)
            {
                string plainText = string.Empty;
                string key = string.Empty;

                foreach (var sequence in sequences)
                {
                    IStrategy strategy;
                    if (!tryAllMostUsedChars && !CharUtils.HasDuplicates(sequence.SequenceText))
                    {
                        strategy = new TopShotStrategy();
                    }
                    else
                    {
                        strategy = new SimpleStrategy(mostUsedCurrentChar.Current.Key);
                    }

                    key += sequence.Analyze(strategy);
                }

                plainText = TryDecrypt(cipherText, key, keyLength);
                //Verify PlainText
                var plausibilityList = new List<double>();

                AnalyzePossiblePlainText(plausibilityList, plainText, frequencyFactor, topShotFactor, bigramFactor, startEndFactor);

                var timeElapsed = stopWatch.ElapsedMilliseconds < TimeSpan.FromMinutes(1.5).TotalMilliseconds;

#if DEBUG
                timeElapsed = false;
#endif

                if (plausibilityList.Sum() < 0.9 && !timeElapsed)
                {
                    tryAllMostUsedChars = true;
                    if (!mostUsedCurrentChar.MoveNext())
                    {
                        cipherDecrypted = true;
                    }
                }
                else
                {
                    cipherDecrypted = true;
                }
                tempDecryptResults.Add(new TempDecryptResult(new DecryptResult() { Key = key, Text = plainText }, plausibilityList.Sum()));
            }

            var tryBruteForceStrategy = true;

            //84% bei BELLENDE mit key DACH

            if (tempDecryptResults.Any())
            {
                tryBruteForceStrategy = (tempDecryptResults.OrderByDescending(t => t.Rate).First().Rate < 0.83);
            }

            if (tryBruteForceStrategy)
            {
                var keyGenerator  = new PossibleKeyGenerator();

                var tempBruteForceDecryptResults = new ConcurrentBag<TempDecryptResult>();

                foreach (var possibleBruteForceKey in keyGenerator.StartBruteForce(keyLength).AsParallel())
                {
                    var timeElapsed = stopWatch.ElapsedMilliseconds < TimeSpan.FromMinutes(1.5).TotalMilliseconds;

#if DEBUG
                    timeElapsed = false;
#endif

                    var plausibilityList = new List<double>();
                    var plainText = TryDecrypt(cipherText, possibleBruteForceKey, keyLength);

                    AnalyzePossiblePlainText(plausibilityList, plainText, frequencyFactor, topShotFactor, bigramFactor, startEndFactor);
                    tempBruteForceDecryptResults.Add(new TempDecryptResult(new DecryptResult() { Key = possibleBruteForceKey, Text = plainText }, plausibilityList.Sum()));

                    if (plausibilityList.Sum() < 0.83 && !timeElapsed)
                    {
                        continue;
                    }
                    else
                    {
                        break;
                    }
                    
                }

               tempBruteForceDecryptResults.ToList().ForEach(tempDecryptResults.Add);
            }

            if (tempDecryptResults.Any())
            {
                return tempDecryptResults.OrderByDescending(t => t.Rate).First().DecryptResult;
            }

            return new DecryptResult() { Key = "NO KEY", Text = cipherText };

        }

        private static void AnalyzePossiblePlainText(List<double> plausibilityList, string plainText, double frequencyFactor,
            double topShotFactor, double bigramFactor, double startEndFactor)
        {
            var freaAnalyzer = new FrequencyAnalyzer();
            plausibilityList.Add(freaAnalyzer.Analyze(plainText)*frequencyFactor);

            var freaAnalyzerTopShot = new FrequencyAnalyzerTopShots();
            plausibilityList.Add(freaAnalyzerTopShot.Analyze(plainText)*topShotFactor);

            var bigramAnalyzer = new BigramAnalyzer();
            plausibilityList.Add(bigramAnalyzer.Analyze(plainText)*bigramFactor);

            var startEndAnalyzer = new StartEndCharacterAnalyzer();
            plausibilityList.Add(startEndAnalyzer.Analyze(plainText)*startEndFactor);

            var dictAnalyzer = new DictAnalyzer();
            plausibilityList.Add(dictAnalyzer.Analyze(plainText));
        }

        public string TryDecrypt(string cipherText, string key, int keyLength)
        {
            var plainText = new StringBuilder();
            int currentKeyPos = 1;
            foreach (var cipherChar in cipherText)
            {
                plainText.Append(CharUtils.GetKey(key[currentKeyPos - 1], cipherChar));
                currentKeyPos++;
                if (currentKeyPos > keyLength)
                {
                    currentKeyPos = 1;
                }
            }

            return plainText.ToString();
        }
    }


}
