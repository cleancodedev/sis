﻿using contest.submission.contract;
using contest.submission.Splitting;

namespace contest.submission.Strategies
{
    public class SimpleStrategy : IStrategy
    {
        private readonly char _mostUsedChar;

        public SimpleStrategy(char mostUsedChar)
        {
            _mostUsedChar = mostUsedChar;
        }

        public char Analyze(string cipherText, int keyLength)
        {
            return CharUtils.GetKey(_mostUsedChar, CharUtils.MostUsedChar(cipherText));
        }
    }
}
