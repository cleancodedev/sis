﻿namespace contest.submission.Strategies
{
    public interface IStrategy
    {
        char Analyze(string cipherText, int keyLength);
    }
}
