﻿using contest.submission.contract;
using contest.submission.Splitting;

namespace contest.submission.Strategies
{
    public class BruteForceStrategy : IStrategy
    {
        public char CurrentChar { get; set; }



        public char Analyze(string cipherText, int keyLength)
        {
            return CharUtils.GetKey(CurrentChar, CharUtils.MostUsedChar(cipherText));
        }
    }
}
