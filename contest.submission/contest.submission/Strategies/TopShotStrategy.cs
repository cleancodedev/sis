﻿using System.Collections.Generic;
using System.Linq;
using contest.submission.Analyzers;

namespace contest.submission.Strategies
{
    public class TopShotStrategy : IStrategy
    {
        public char Analyze(string sequenceCipherText, int keyLength)
        {
            var result = new Dictionary<char, double>();
            for (int i = 0; i < sequenceCipherText.Length; i++)
            {
                var possibleKeyChar = CharUtils.GetKey('E', sequenceCipherText[i]);

                var possiblePlainText = CharUtils.GetPlainText(sequenceCipherText, possibleKeyChar);

                FrequencyAnalyzerTopShots fats = new FrequencyAnalyzerTopShots();
                result.Add(possibleKeyChar, fats.Analyze(possiblePlainText));
            }

            return result.OrderByDescending(t => t.Value).First().Key;
        }

    }
}
