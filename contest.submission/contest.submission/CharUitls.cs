using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace contest.submission
{
    public static class CharUtils
    {
        private const char minChar = 'A';
        private const char maxChar = 'Z';
        private const int possibleChars = maxChar - minChar + 1;

        public static int GetOffset(char fromChar, char toChar)
        {
            return toChar - fromChar;
        }

        public static char TransformCharacter(char fromChar, int offset)
        {
            return (char)(fromChar + offset);
        }

        public static string WriteBruteStrings(string prefix, char fromChar, char toChar, int keyLength)
        {
            if (prefix.Length == keyLength)
                return prefix;

            for (char c = fromChar; c <= toChar; c++)
            {
                prefix += WriteBruteStrings(prefix + c, fromChar,toChar, keyLength);
            }
                
            return prefix;

        }

        public static IEnumerable<string> GetBruteForceKeys(char fromChar, char toChar, int keyLength)
        {
            var key = string.Empty;
            yield return WriteBruteStrings(key, fromChar, toChar, keyLength);
        }


        public static char GetKey(char plainText, char cipherText)
        {
            int offset = GetOffset(plainText, cipherText);
            if (offset < 0)
            {
                offset = possibleChars + offset;
            }

            return (char)(minChar + offset);
        }

        internal static bool HasDuplicates(string text)
        {
            return text.GroupBy(x => x).Any(x => x.Count() >1);
        }

        public static int CountChar(string text, char searchChar)
        {
            return text.ToCharArray().Where(t => t.Equals(searchChar)).Count();
        }

        public static int CountString(string text, string searchString)
        {
            return Regex.Matches(text, searchString).Count;
        }

        public static char MostUsedChar(string sequence)
        {
            return sequence.GroupBy(x => x).OrderByDescending(x => x.Count()).First().Key;
        }

        public static string GetPlainText(string cipherText, char key)
        {
            string plainText = string.Empty;
            foreach (var cipherChar in cipherText)
            {
                plainText += GetKey(key, cipherChar);
                
            }
            return plainText;
        }
    }
}