﻿using System;
using contest.submission.Splitting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;

namespace contest.submission.tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var splitter = new Splitter();
            var result = splitter.Split("abcdefg", 3);
            result.Length.Should().Be(3);
 
            result.Should().ContainInOrder("adg", "be", "cf");
        }

        [TestMethod]
        public void SplittNull()
        {
            var splitter = new Splitter();
            var result = splitter.Split(null, 3);
            result.Should().BeEmpty();
        }
    }
}
