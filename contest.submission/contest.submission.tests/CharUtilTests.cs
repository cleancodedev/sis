using System.Linq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace contest.submission.tests
{
    [TestClass]
    public class CharUtilTests
    {

        [TestMethod]
        public void GetOffset_InputValuesWithAPositiveOffset_ShouldReturnTheExpecetedOffset()
        {
            //Arrange

            //Act
            var offset = CharUtils.GetOffset('C', 'D');

            //Assert
            offset.Should().Be(1);
        }

        [TestMethod]
        public void GetOffset_InputValuesWithANegativeOffset_ShouldReturnTheExpecetedOffset()
        {
            //Arrange

            //Act
            var offset = CharUtils.GetOffset('B', 'A');

            //Assert
            offset.Should().Be(-1);
        }

        [TestMethod]
        public void GetOffset_InputValuesWithANegativeOffset_ShouldReturnTheExpecetedOffset2()
        {
            //Arrange

            //Act
            var offset = CharUtils.GetOffset('N', 'M');

            //Assert
            offset.Should().Be(-1);
        }

        [TestMethod]
        public void GetOffset_InputSameValues_ShouldReturn0Offset()
        {
            //Arrange


            //Act
            var offset = CharUtils.GetOffset('A', 'A');

            //Assert
            offset.Should().Be(0);
        }

        [TestMethod]
        public void Transform_InputVariousChar_ShouldTranformToExcpectedChars()
        {
            //Arrange

            //Assert
            CharUtils.TransformCharacter('A', 1).Should().Be('B');
            CharUtils.TransformCharacter('A', 25).Should().Be('Z');
            CharUtils.TransformCharacter('B', -1).Should().Be('A');
            CharUtils.TransformCharacter('Z', -25).Should().Be('A');
        }

        [TestMethod]
        public void CharCount_InputText_ShouldReturnExpectedCount()
        {
            //Arrange

            //Assert
            CharUtils.CountChar("AAA", 'A').Should().Be(3);
            CharUtils.CountChar("AEE", 'E').Should().Be(2);
            CharUtils.CountChar("POLKIASZZDFMACSKJAS", 'Z').Should().Be(2);
        }

        [TestMethod]
        public void StringCount_InputText_ShouldReturnExpectedCount()
        {
            //Arrange

            //Assert
            CharUtils.CountString("AAA", "A").Should().Be(3);
            CharUtils.CountString("AEE", "E").Should().Be(2);
            CharUtils.CountString("POLKIASZZDFMACZZSKJAS","ZZ").Should().Be(2);
            CharUtils.CountString("POLKIASZZDFMACZZSKJAS", "T").Should().Be(0);
            CharUtils.CountString("POLKIASZZDFMACZZSKJAS", "IT").Should().Be(0);
        }


        [TestMethod]
        public void GetKey()
        {
            CharUtils.GetKey('B', 'F').Should().Be('E');
            CharUtils.GetKey('F', 'B').Should().Be('W');
        }

        [TestMethod]
        public void FindKeyWithMostE()
        {
            CharUtils.MostUsedChar("abcccdd").Should().Be('c');
            CharUtils.MostUsedChar("aacc").Should().Be('a');
        }

        [TestMethod]
        public void Decipher()
        {
            var cipherText = "SLKPUO";

            string result = string.Empty;
            foreach (var d in cipherText)
            {
                result += CharUtils.GetKey('H', d);
            }

            int i = 0;
         
        }


       
        [TestMethod]
        public void CreateBruteForceKeys()
        {
            var keys = CharUtils.GetBruteForceKeys('A', 'C', 2).ToList();

            keys.Should().HaveCount(6);



        }
    }
}