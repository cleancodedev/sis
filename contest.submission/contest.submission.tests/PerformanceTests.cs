﻿using System;
using System.Diagnostics;
using contest.submission.contract;
using contest.submission.tests.Utils;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace contest.submission.tests
{
    [TestClass]
    public class PerformanceTests
    {
        [TestMethod]
        public void Scenario1_AThousandTimes()
        {
            var stopwatch = new Stopwatch();
            int count = 0;

            DecryptResult result = null;
            var solution = new Solution();

            solution.Result += r =>
            {
                count++;
                if (count >= 1000)
                {
                    stopwatch.Stop();
                    result = r;
                }
            };

            stopwatch.Start();
            for (int i = 0; i < 1000; i++)
            {
                solution.Process(InputAndOutputs.Scenario2Input, 4);
            }

            Console.Out.WriteLine("Measurement: {0} ms", stopwatch.ElapsedMilliseconds);
            result.Key.Should().Be("DACH");
            result.Text.Should().Be(InputAndOutputs.Scenario2Output);
        }
    }
}