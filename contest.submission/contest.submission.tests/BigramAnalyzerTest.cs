﻿using contest.submission.Analyzers;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace contest.submission.tests
{
    [TestClass]
    public class BigramAnalyzerTest
    {
        private BigramAnalyzer _unitUnderTest;

        [TestMethod]
        public void GetOffset_InputLongText_Expected()
        {
            //Arrange
            _unitUnderTest = new BigramAnalyzer();

            //Act
            var offset = _unitUnderTest.Analyze("ALSSOHNDESSALAMINISCHENKOENIGSTELAMONUNDDERPERIBOIAWURDEERDERTELAMONIERGENANNTAIASWAREINMAECHTIGERGRIECHISCHERKAEMPFERVORTROJANURACHILLEUSUEBERTRAFIHNNACHHOMERSILIASWARAIASRIESIGVIELGROESSERALSANDEREMAENNERFASTGLEICHEINERMAUERDIEQUELLEBETONTEINENUNTERDENRUHMSUECHTIGENUNDBEUTEGIERIGENHELDENUNGEWOEHNLICHENZUGANIHMNAEMLICHDASSIHNMITSEINERBEUTESKLAVINTEKMESSAEINESTARKEGEGENSEITIGELIEBEVERBANDAUSDERAUCHEINSOHNEURYSAKESHERVORGINGALSACHILLEUSSICHVOMKAMPFZURUECKZOGWURDEAIASAUSGELOSTUMHEKTORDEMSTAERKSTENTROJANERIMZWEIKAMPFGEGENUEBERZUTRETENDERKAMPFDAUERTEDENGANZENTAGHEKTORWURDELEICHTVERLETZTWORAUFHINSICHDIEHELDENINHOHERWECHSELSEITIGERACHTUNGTRENNTEN");

            //Assert
            offset.Should().Be(0.02324);
        }
    }
}
