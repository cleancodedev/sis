﻿using contest.submission.tests.Utils;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace contest.submission.tests
{
    [TestClass]
    public class DecryptorTests
    {
        [TestMethod]
        public void DecryptMerkel()
        {
            var decryptor = new Decryptor();

            var result = decryptor.TryDecrypt(InputAndOutputs.Scenario7Input, InputAndOutputs.Scenario7Key,
                InputAndOutputs.Scenario7Key.Length);

            result.Should().Be(InputAndOutputs.Scenario7Output.ToUpper());
        }

        [TestMethod]
        public void DecryptBellendeHunde()
        {
            var decryptor = new Decryptor();

            var result = decryptor.TryDecrypt(InputAndOutputs.Scenario1Input, InputAndOutputs.Scenario1Key,
                InputAndOutputs.Scenario1Key.Length);

            result.Should().Be(InputAndOutputs.Scenario1Output.ToUpper());
        }
    }
}
