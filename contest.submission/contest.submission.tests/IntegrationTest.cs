﻿using System;
using System.Linq;
using contest.submission.Analyzers;
using contest.submission.tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using contest.submission.contract;
using FluentAssertions;

namespace contest.submission.tests
{
    [TestClass]
    public class IntegrationTest
    {
        string input3 = "BELLENDEHUNDEBEISSENNICHT";
        string output3 = "EENSHNFLKUPKHBGPVSGUQIEOW";

        string aiasOutput = "ALSSOHNDESSALAMINISCHENKOENIGSTELAMONUNDDERPERIBOIAWURDEERDERTELAMONIERGENANNTAIASWAREINMAECHTIGERGRIECHISCHERKAEMPFERVORTROJANURACHILLEUSUEBERTRAFIHNNACHHOMERSILIASWARAIASRIESIGVIELGROESSERALSANDEREMAENNERFASTGLEICHEINERMAUERDIEQUELLEBETONTEINENUNTERDENRUHMSUECHTIGENUNDBEUTEGIERIGENHELDENUNGEWOEHNLICHENZUGANIHMNAEMLICHDASSIHNMITSEINERBEUTESKLAVINTEKMESSAEINESTARKEGEGENSEITIGELIEBEVERBANDAUSDERAUCHEINSOHNEURYSAKESHERVORGINGALSACHILLEUSSICHVOMKAMPFZURUECKZOGWURDEAIASAUSGELOSTUMHEKTORDEMSTAERKSTENTROJANERIMZWEIKAMPFGEGENUEBERZUTRETENDERKAMPFDAUERTEDENGANZENTAGHEKTORWURDELEICHTVERLETZTWORAUFHINSICHDIEHELDENINHOHERWECHSELSEITIGERACHTUNGTRENNTEN";
        string aiasInput =
            @"DLUZRHPKHSUHOAOPQIUJKEPRREPPJSVLOAOVQUPKGETWHRKIRICDXRFLHRFLUTGSDMQULETNHNCUQTCP" +
             "DSYHUEKUPAGJKTKNHRIYLEEOLSEOHRMHHMRMHRXVUTTVMAPBUAEOLLNLXSWLEETAUAHPKNPHFHJVPETZ" +
             "LLKHVWCYDICZUIGZLGXPHLIYREUZHRCSVAPKHRGTDEPUHRHHVTISHIEOHIPLUMCBHRFPHQWLOLGIHTQU" +
             "WEKUHNWUWETKHNTBKMUBHCJALGGUXNFIHUVLJIGYLGGUKENKHNWUJEYVHHPSLCJLQZWNDNKOPNCLPLKJ" +
             "KDCZVIJUPIVZHIPLUBGBWEUROAXPQTGRPEUZDEKUHSVHUKGNHGGUVEKALGGSLEDLYETIDNFHXSFLUAWJ" +
             "KEKUVOJUHUTFVAMLVHGYYOTNLNIHOSCJKINSHUUZLCJCRMMHPPHGXRWLFKBVJWWYGECPDSCBVGGSRSVB" +
             "PHGRWOTKHMUADETRVTGUWRQQDNGYLMBDHIMHPPHNHGGUXEDLUZWAUEVLQDGYNAOWIDCBHRVLGEPNDNBL" +
             "QTCNKEMARRYBUDGSHIEOWVGYOEVGWWQYDUHOLNUPFHFPHHGSGEPPQHQOHRYLFHULOSGPWIILUAEOWUPN" +
             "WRGUQTGU";

        [TestMethod]
        public void DecipherBellendeHunde()
        {
            DecryptResult result = null;
            var solution = new Solution();
            solution.Result += r => result = r;
            solution.Process(InputAndOutputs.Scenario1Input, 4);

            result.Key.Should().Be(InputAndOutputs.Scenario1Key);
            result.Text.Should().Be(InputAndOutputs.Scenario1Output);
        }

        [TestMethod]
        public void DecipherGlatze()
        {
            DecryptResult result = null;
            var solution = new Solution();
            solution.Result += r => result = r;
            solution.Process(InputAndOutputs.Scenario4Input, InputAndOutputs.Scenario4Key.Length);

            result.Key.Should().Be(InputAndOutputs.Scenario4Key);
            result.Text.Should().Be(InputAndOutputs.Scenario4Output);
        }


        [TestMethod]
        public void DecipherDeutschland()
        {
            DecryptResult result = null;
            var solution = new Solution();
            solution.Result += r => result = r;
            solution.Process(InputAndOutputs.Scenario5Input, InputAndOutputs.Scenario5Key.Length);

            result.Key.Should().Be(InputAndOutputs.Scenario5Key);
            result.Text.Should().Be(InputAndOutputs.Scenario5Output);
        }

        [TestMethod]
        public void DecipherPrinzSaeufer()
        {
            DecryptResult result = null;
            var solution = new Solution();
            solution.Result += r => result = r;
            solution.Process(InputAndOutputs.Scenario6Input, InputAndOutputs.Scenario6Key.Length);

            result.Key.Should().Be(InputAndOutputs.Scenario6Key);
            result.Text.Should().Be(InputAndOutputs.Scenario6Output);
        }

        [TestMethod]
        public void DecipherMerkel()
        {
            DecryptResult result = null;
            var solution = new Solution();
            solution.Result += r => result = r;
            solution.Process(InputAndOutputs.Scenario7Input, InputAndOutputs.Scenario7Key.Length);

            result.Key.Should().Be(InputAndOutputs.Scenario7Key);
            result.Text.Should().Be(InputAndOutputs.Scenario7Output.ToUpper());
        }

        [TestMethod]
        public void DecipherAiasKey()
        {
            DecryptResult result = null;
            var solution = new Solution();
            solution.Result += r => result = r;
            solution.Process(InputAndOutputs.Scenario2Input, 4);

            result.Key.Should().Be(InputAndOutputs.Scenario1Key);
            result.Text.Should().Be(InputAndOutputs.Scenario2Output);
        }

        [TestMethod]
        public void DecipherHausIstBlauKey()
        {
            DecryptResult result = null;
            var solution = new Solution();
            solution.Result += r => result = r;

            solution.Process(InputAndOutputs.Scenario3Input, 4);

            result.Key.Should().Be(InputAndOutputs.Scenario1Key);
            result.Text.Should().Be(InputAndOutputs.Scenario3Output);
        }

        [TestMethod]
        public void DecipherAiasReduceText()
        {
            var length = aiasInput.Length;
            for (int i = 0; i < aiasInput.Length; i++)
            {
                DecryptResult result = null;
                var solution = new Solution();
                solution.Result += r => result = r;
                solution.Process(InputAndOutputs.Scenario2Input.Substring(0, length - i), 4);

                // 113 zeichen resp 113/4 = 28.25 somit 29 zeichen
                result.Key.Should().Be(InputAndOutputs.Scenario1Key, "because {0}", length - i);
                result.Text.Should().Be(InputAndOutputs.Scenario2Output.Substring(0, length - i));
            }
        }
    }
}
