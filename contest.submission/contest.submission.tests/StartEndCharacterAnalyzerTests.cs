using contest.submission.Analyzers;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace contest.submission.tests
{
    [TestClass]
    public class StartEndCharacterAnalyzerTests
    {
        private StartEndCharacterAnalyzer _unitUnderTest;

        [TestMethod]
        public void GetOffset_InputShortText_Expected()
        {
            //Arrange
            _unitUnderTest = new StartEndCharacterAnalyzer();

            //Act
            var offset = _unitUnderTest.Analyze("BELLENDEHUNDEBEISSENNICHT");

            //Assert
            offset.Should().Be(0.103);
        }
    }
}