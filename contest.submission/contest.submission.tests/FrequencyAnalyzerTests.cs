using System;
using contest.submission.Analyzers;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace contest.submission.tests
{
    [TestClass]
    public class FrequencyAnalyzerTests
    {
        private FrequencyAnalyzer _unitUnderTest;

        [TestMethod]
        public void GetOffset_InputShortText_Expected()
        {
            //Arrange
            _unitUnderTest = new FrequencyAnalyzer();

            //Act
            var offset = _unitUnderTest.Analyze("BELLENDEHUNDEBEISSENNICHT");

            //Assert
            offset.ToString().Should().Be("0.1917");
        }

        [TestMethod]
        public void GetOffset_InputLongText_Expected()
        {
            //Arrange
            _unitUnderTest = new FrequencyAnalyzer();

            //Act
            var offset = _unitUnderTest.Analyze("ALSSOHNDESSALAMINISCHENKOENIGSTELAMONUNDDERPERIBOIAWURDEERDERTELAMONIERGENANNTAIASWAREINMAECHTIGERGRIECHISCHERKAEMPFERVORTROJANURACHILLEUSUEBERTRAFIHNNACHHOMERSILIASWARAIASRIESIGVIELGROESSERALSANDEREMAENNERFASTGLEICHEINERMAUERDIEQUELLEBETONTEINENUNTERDENRUHMSUECHTIGENUNDBEUTEGIERIGENHELDENUNGEWOEHNLICHENZUGANIHMNAEMLICHDASSIHNMITSEINERBEUTESKLAVINTEKMESSAEINESTARKEGEGENSEITIGELIEBEVERBANDAUSDERAUCHEINSOHNEURYSAKESHERVORGINGALSACHILLEUSSICHVOMKAMPFZURUECKZOGWURDEAIASAUSGELOSTUMHEKTORDEMSTAERKSTENTROJANERIMZWEIKAMPFGEGENUEBERZUTRETENDERKAMPFDAUERTEDENGANZENTAGHEKTORWURDELEICHTVERLETZTWORAUFHINSICHDIEHELDENINHOHERWECHSELSEITIGERACHTUNGTRENNTEN");

            //Assert
            offset.Should().Be(0.8532);
        }


        [TestMethod]
        public void AnalyzeBellendeHunde()
        {
            //Arrange
            var _unitUnderTest = new FrequencyAnalyzerTopShots();

            //Act
            var offset1 = _unitUnderTest.Analyze("EXWBGA");
            var offset2 = _unitUnderTest.Analyze("LEDNIH");


            //Assert
            offset1.Should().BeLessThan(offset2);
        }

        [TestMethod]
        public void GetOffset_InputLongEnglishText()
        {
            //Arrange
            _unitUnderTest = new FrequencyAnalyzer();

            //Act
            var offset = _unitUnderTest.Analyze("INHOMERSILIADHEISDESCRIBEDASOFGREATSTATURECOLOSSALFRAMEANDSTRONGESTOFALLTHEACHAEANSKNOWNASTHEBULWARKOFTHEACHAEANSHEWASTRAINEDBYTHECENTAURCHIRON(WHOHADTRAINEDAJAX'FATHERTELAMONANDACHILLES'FATHERPELEUSANDWOULDLATERDIEOFANACCIDENTALWOUNDINFLICTEDBYHERACLESWHOMHEWASATTHETIMETRAINING)ATTHESAMETIMEASACHILLESHEWASDESCRIBEDASFEARLESSSTRONGANDPOWERFULBUTALSOWITHAVERYHIGHLEVELOFCOMBATINTELLIGENCEAJAXCOMMANDSHISARMYWIELDINGAHUGESHIELDMADEOFSEVENCOW-HIDESWITHALAYEROFBRONZEMOSTNOTABLYAJAXISNOTWOUNDEDINANYOFTHEBATTLESDESCRIBEDINTHEILIADANDHEISTHEONLYPRINCIPALCHARACTERONEITHERSIDEWHODOESNOTRECEIVESUBSTANTIALASSISTANCEFROMANYOFTHEGODSWHOTAKEPARTINTHEBATTLESALTHOUGHINBOOK13POSEIDONSTRIKESAJAXWITHHISSTAFFRENEWINGHISSTRENGTH");

            //Assert
            offset.Should().Be(6);
        }
    }
}