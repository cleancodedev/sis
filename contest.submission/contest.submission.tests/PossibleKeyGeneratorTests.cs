using System.Linq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace contest.submission.tests
{
    [TestClass]
    public class PossibleKeyGeneratorTests
    {
        private PossibleKeyGenerator _unitUnderTest;



       
        [TestMethod]
        public void CreateBruteForceKeys()
        {

            _unitUnderTest = new PossibleKeyGenerator();

            var keys = _unitUnderTest.StartBruteForce(2).ToList();

            keys.Should().HaveCount(676);



        }
    }
}