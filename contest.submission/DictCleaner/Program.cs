﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace DictCleaner
{
    class Program
    {
        static void Main(string[] args)
        {
            Uri filePath = new Uri(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "german.txt"));

            string text = File.ReadAllText(filePath.AbsolutePath);

            text = text.ToUpper();

            text = text.Replace("Ü", "UE");
            text = text.Replace("Ö", "OE");
            text = text.Replace("Ä", "AE");
            text = text.Replace("ß", "SS");

            text = text.Replace("\n", Environment.NewLine);

            var textItems = text.Split(new[] {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries);
            var cleanItems = textItems.Where(t => t.Length > 3).ToList();

            cleanItems.Insert(0,"DIE");
            cleanItems.Insert(1, "DER");
            cleanItems.Insert(2, "MIT");
            cleanItems.Insert(3, "IST");

            //validate
            Regex rgx = new Regex("[^A-Z]");
            var invalid = cleanItems.Where(t => rgx.IsMatch(t));

            var finalList = cleanItems.Except(invalid);

            //var invalid = cleanItems.Where(t => (t < 'A' || t > 'Z') && (t != '\r' || t != '\n')).ToList();

            var outputText = String.Join(Environment.NewLine, finalList).ToUpper();

            var newFilePath = filePath.AbsolutePath + ".clean";
            if (File.Exists(newFilePath))
            {
                File.Delete(newFilePath);
            }

            File.WriteAllText(newFilePath, outputText);
        }
    }
}
