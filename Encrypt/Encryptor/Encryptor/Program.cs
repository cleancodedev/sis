﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
  

namespace Encryptor
{
  class Program
  {
    static void Main(string[] args)
    {
      string alignedtext = Cryptor.Align(args[0]);
      
      Console.WriteLine(
        Cryptor.Encrypt(alignedtext, 
          Cryptor.MultiplyKey(alignedtext, args[1])
        )
      );

      Console.ReadKey();
    }
  }

  public class Cryptor
  {
    
    public static string Encrypt(string text, string multiplykey)
    {
      StringBuilder sb = new StringBuilder();
      for (int i = 0; i < text.Length; i++)
      {
        int cToEncrypt = (int) text[i] - 65;
        int cKey = (int) multiplykey[i] - 65;

        sb.Append((char) (((cToEncrypt + cKey) % 26) + 65));
      }
      
      var result = sb.ToString();

        if (result.Length != text.Length)
        {
            throw new ArgumentException("cipher and plaintext have different length!");
        }

        return result;
    }

    public static string MultiplyKey(string text, string key)
    {
      StringBuilder sb = new StringBuilder();
      for (int i = 0; i < (text.Length / key.Length); i++)
      {
        sb.Append(key);
      }
      sb.Append(key.Substring(0, text.Length % key.Length));
      return sb.ToString();
    }

    public static string Align(string text)
    {
      string Text = text;
      Regex pattern = new Regex("[.;,\t\r ]");
      Text = pattern.Replace(text, "");
      return Text.ToUpper();
    }
  }


}
