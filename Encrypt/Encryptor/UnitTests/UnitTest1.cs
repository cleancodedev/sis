﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Encryptor;


namespace UnitTests
{

  [TestClass]
  public class Tests
    {
        


        [TestMethod]
    public void Check_Encrypt()
    {
      string key = "DACH";

      string input1 = "A";
      string output1 = "D";

      string input2 = "BELL";
      string output2 = "EENS";

      string input3 = "BELLENDEHUNDEBEISSENNICHT";
      string output3 = "EENSHNFLKUPKHBGPVSGUQIEOW";

      string input4 = "WXYZ";
      string output4 = "ZXAG";

      string key5 = "XYZZ";
      string input5 = "AAAB";
      string output5 = "XYZA";

            string key6 = "DACH";
            string input6 = "DASHAUSISTBLAU";
            string output6 = "GAUODUUPVTDSDU";

    

      //ABCDEFGHIJKLMNOPQRSTUVWXYZ 
      //01234567890123456789012345
            Assert.AreEqual(output1, Cryptor.Encrypt(input1, Cryptor.MultiplyKey(input1, key)), "Stimmen nicht überein");
      Assert.AreEqual(output2, Cryptor.Encrypt(input2, Cryptor.MultiplyKey(input2, key)), "Stimmen nicht überein");
      Assert.AreEqual(output3, Cryptor.Encrypt(input3, Cryptor.MultiplyKey(input3, key)), "Stimmen nicht überein");
      Assert.AreEqual(output4, Cryptor.Encrypt(input4, Cryptor.MultiplyKey(input4, key)), "Stimmen nicht überein");
      Assert.AreEqual(output5, Cryptor.Encrypt(input5, Cryptor.MultiplyKey(input5, key5)), "Stimmen nicht überein");
            Assert.AreEqual(output6, Cryptor.Encrypt(input6, Cryptor.MultiplyKey(input6, key6)), "Stimmen nicht überein");
        }

    [TestMethod]
    public void Check_Alignment()
    {
      string input = "Hier steht der erste Text.";
      string output = "HIERSTEHTDERERSTETEXT";
      Assert.AreEqual(output, Cryptor.Align(input), "Stimmen nicht überein");
    }

    [TestMethod]
    public void Check_Key_Multiply1()
    {
      string key = "DACH";

      string input1  = "HIERSTEHTDERERSTETEXT";
      string output1 = "DACHDACHDACHDACHDACHD";

      string input2 = "HIERSTEHTDERERSTETEX";
      string output2 = "DACHDACHDACHDACHDACH";

      string input3 = "HIERSTEHTDERERSTETEXTER";
      string output3 = "DACHDACHDACHDACHDACHDAC";

      string input4 = "HIE";
      string output4 = "DAC";

      string input5 = "";
      string output5 = "";

      Assert.AreEqual(output1, Cryptor.MultiplyKey(input1, key), "Stimmen nicht überein");
      Assert.AreEqual(output2, Cryptor.MultiplyKey(input2, key), "Stimmen nicht überein");
      Assert.AreEqual(output3, Cryptor.MultiplyKey(input3, key), "Stimmen nicht überein");
      Assert.AreEqual(output4, Cryptor.MultiplyKey(input4, key), "Stimmen nicht überein");
      Assert.AreEqual(output5, Cryptor.MultiplyKey(input5, key), "Stimmen nicht überein");
    }

  }
}
