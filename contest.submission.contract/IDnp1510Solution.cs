﻿using System;

namespace contest.submission.contract
{
  public class DecryptResult
  {
    public string Text { get; set; }
    public string Key { get; set; }
  }

    public interface IDnp1510Solution
    {
      void Process(string text, int keylength);

      event Action<DecryptResult> Result;
    }
}
